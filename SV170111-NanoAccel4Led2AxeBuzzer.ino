#include<Wire.h>

const int MPU=0x68, AXEX=0x3B, AXEY=0x3D, AXEZ=0x3F;// I2C address of the MPU-6050
const int LED_AR_GAUCHE=4, LED_AR_DROITE=3, LED_AV_GAUCHE = 6, LED_AV_DROITE = 7, BUZZ = 14; // Pins du montage, en vrai Buzz=13
const int LENT=300, RAPIDE=100, FREIN=500, NOTE_CLIGNO_G=440, NOTE_CLIGNO_D=660;  // constantes
const int SEUIL_FREIN=7000, SEUIL_ROT=8000; // constantes
long AcX,AcY,AcZ, GyX, GyY, GyZ; //lectures de l accelero
long AcX0,AcY0,AcZ0; // offsets d initialisation
int i=0, serie=0; //compteur de boucle, de serie de logs
bool etatLED = 0;

unsigned long DUREE_CYCLE = 250 ; // Duree du cycle de lecture
unsigned long tempsLog = 0 , debutLog ; // temps du fichier de log

void setup(){
  delay(3000); //Attente position de conduite pour initialisation
  /* Initialisation de l'accéléro */
  Serial.begin(115200);
  Serial.print("Initializing ...");
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B); // PWR_MGMT_1 register
  Wire.write(0); // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(false);
    Serial.println("Lecture Axe X ...");
  AcX0=moyenneAccelero(AXEX, 100);
    Serial.println("Lecture Axe Y ...");
  AcY0=moyenneAccelero(AXEY, 100);
  Wire.endTransmission(true);

  
/* Initialisation */

  Serial.print(" AcX0 = "); Serial.print(AcX0);
  Serial.print(" AcY0 = "); Serial.println(AcY0);

   pinMode(LED_AR_GAUCHE, OUTPUT);  // LED
   pinMode(LED_AR_DROITE, OUTPUT);  // LED
   pinMode(LED_AV_GAUCHE, OUTPUT);  // LED
   pinMode(LED_AV_DROITE, OUTPUT);  // LED
   pinMode(BUZZ, OUTPUT);  // Buzzer     
  
  Serial.println("initialization done.");
        digitalWrite(LED_AR_GAUCHE, HIGH);
        digitalWrite(LED_AV_GAUCHE, HIGH);
        tone(BUZZ, NOTE_CLIGNO_G,500);
        digitalWrite(LED_AR_GAUCHE, LOW);
        digitalWrite(LED_AV_GAUCHE, LOW);
        delay(500); 
        digitalWrite(LED_AR_DROITE, HIGH);
        digitalWrite(LED_AV_DROITE, HIGH);
        tone(BUZZ, NOTE_CLIGNO_D,500);
        digitalWrite(LED_AR_DROITE, LOW);
        digitalWrite(LED_AV_DROITE, LOW);
        
}

void loop(){

        etatLED=!etatLED;
        digitalWrite(LED_AR_GAUCHE, etatLED);
        digitalWrite(LED_AR_DROITE, etatLED);
        digitalWrite(LED_AV_GAUCHE, !etatLED);
        digitalWrite(LED_AV_DROITE, !etatLED);

        Wire.beginTransmission(MPU);
        Wire.write(AXEX); // REGISTRE Y //starting with register 0x46 (GYRO_YOUT_H)
        Wire.endTransmission(false);
        Wire.requestFrom(MPU,4,true); // request a total of 6 registers
        AcX=Wire.read()<<8|Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L) */
        AcX = AcX-AcX0;   
        AcY=Wire.read()<<8|Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
        AcY = AcY-AcY0;
        //AcZ=Wire.read()<<8|Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)        
        //Wire.write(0x3F); // REGISTRE Z //starting with register 0x3B (ACCEL_XOUT_H)

            
      
        tempsLog = millis() - debutLog;   
        Serial.print(serie); Serial.print(" "); Serial.print(tempsLog);  Serial.print(" AcX = "); Serial.print(AcX);Serial.print(" AcY = "); Serial.print(AcY);
                Serial.print(" AcZ = "); Serial.println(AcZ);
        //Serial.print(" | AcY = "); Serial.print(AcY);
        //Serial.print(" | AcZ = "); Serial.println(AcZ);

    //Priorite a la detection des tournants sur le freinage        
         if (AcX > SEUIL_ROT) {
              Serial.println("ACC DROITE");
              digitalWrite(LED_AR_GAUCHE, LOW);
              digitalWrite(LED_AV_GAUCHE, LOW);
              blinkDouble(LED_AR_DROITE, LED_AV_DROITE, LENT, NOTE_CLIGNO_D);
              //digitalWrite(LED_AR_GAUCHE, HIGH);
         }
         else if (AcX < -SEUIL_ROT){
              Serial.println("ACC GAUCHE");           
              digitalWrite(LED_AR_DROITE, LOW);
              digitalWrite(LED_AV_DROITE, LOW);
              blinkDouble(LED_AR_GAUCHE, LED_AV_GAUCHE, LENT, NOTE_CLIGNO_G);
              //digitalWrite(LED_AR_DROITE, HIGH);
         }
         else if (AcY > SEUIL_FREIN) {
              Serial.println("ACC FREINAGE");
              ledFreinage(LED_AR_GAUCHE, LED_AR_DROITE, FREIN);
         }
        else {
          delay(DUREE_CYCLE);
        }
  }
  
 void blinkUnique(int pin, int t, int note){
      for (int i = 1; i < 6; ++i) {
            digitalWrite(pin, HIGH); delay(t);
            tone(BUZZ, note,t);
            digitalWrite(pin, LOW); delay(t); 
      }
  }
 
 void blinkDouble(int pin, int pin2, int t, int note){
 
            for (int i = 1; i < 6; ++i) {
              digitalWrite(pin, HIGH);digitalWrite(pin2, HIGH);delay(t);
              tone(BUZZ, note,t);
              digitalWrite(pin, LOW);digitalWrite(pin2, LOW); delay(t); 
            }
    
  }
   void ledFreinage(int pin, int pin2, int t){
 
            digitalWrite(pin, HIGH);digitalWrite(pin2, HIGH);
            tone(BUZZ, NOTE_CLIGNO_D,t);
            digitalWrite(pin, LOW);digitalWrite(pin2, LOW);
   }
  
  
 long moyenneAccelero (int axe, int duree){
    int periode = duree/10;
    long sommeLectures =0;
    for (int j=0;j<periode;j++)
    {
      Wire.beginTransmission(MPU);
      Wire.write(axe); // Starting with register axe 
      Wire.endTransmission(false);
      Wire.requestFrom(MPU,2,true); // request a total of 2 registers
      sommeLectures += Wire.read()<<8|Wire.read(); // High and Low registers
      Serial.println(sommeLectures);
      delay(periode);
    }
    
    return sommeLectures/10;
    
  }

