# SignalVelo

Casque de vélo lumineux, basé sur Arduino.
Projet réalisé au hackerspace LaBarhack : https://www.barhack.fr/projets

## Matériel
 - Des LED monochromes 5V, en ruban étanche
    - 2 rubans blancs à l'avant (G+D)
    - 2 rubans rouges à l'arrière (G+D)
 - Un microcontroleur arduino (Nano)
 - un accéléromètre MPU6050
 - un buzzer en courant continu
 - Un lecteur de carte SD (pour la calibration)

## Code
Le fichier "SV170111-NanoAccel4Led2AxeBuzzer" contient le code final, calibré pour un montage en sommet de casque sans carte SD.
Le fichier "SV160706-NanoSansMenu" contient le code pour calibrer le capteur en enregistrant les données du capteur sur une carte SD.

## Ressources
Code réalisé grâce au travail de rétroingéniérie du capteur MPU6050, placé sous licence MIT, et disponible à l'adresse : https://github.com/jrowberg/i2cdevlib
