#include<Wire.h>
#include <SD.h>

const int MPU=0x68; // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ;
int i=0, serie=0; //compteur de boucle, de serie de logs
unsigned long duree =400 , cycle=50 ; // nombre de cycles de log, duree du cycle
unsigned long tempsLog =0 , debutLog ; // temps du fichier de log
File myFile; // variable du fichier de log
String LogName; // chaine de nom du fichier de log
char* LogFile = "velo2.txt" , junk; // nom du fichier de log ; dump du buffer Serial

void setup(){
/* Initialisation de l'accéléro */
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B); // PWR_MGMT_1 register
  Wire.write(0); // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

  Serial.begin(9600);
  
/* Initialisation de la carte SD */
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(10, OUTPUT);  // LED d'affichage
     
  
   
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    digitalWrite(10, HIGH);
    return;
  }
  Serial.println("initialization done.");
        digitalWrite(10, HIGH);
        delay(1000);
        digitalWrite(10, LOW);
        delay(1000); 
}

void loop(){

      //Ouverture du fichier (un seul à la fois)
      myFile = SD.open(LogFile, FILE_WRITE);
      
      if (myFile) {
        
        digitalWrite(10, HIGH);
        delay(500);
        digitalWrite(10, LOW);
        delay(500);  
        digitalWrite(10, HIGH);
        delay(500);
        digitalWrite(10, LOW);   
        delay(500);  
        digitalWrite(10, HIGH);
        delay(500);
        digitalWrite(10, LOW);   

      } else {
        Serial.print("erreur d'ouverture de ");Serial.println(LogFile);
        digitalWrite(10, HIGH);
        return;
      }
      
      i=0;
      serie++;
      debutLog = millis();
      while (i<duree){
        Wire.beginTransmission(MPU);
        Wire.write(0x3D); // REGISTRE Y //starting with register 0x3B (ACCEL_XOUT_H)
        Wire.endTransmission(false);
        Wire.requestFrom(MPU,2,true); // request a total of 6 registers
        AcX=Wire.read()<<8|Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
       // AcY=Wire.read()<<8|Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
       //AcZ=Wire.read()<<8|Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
      
        tempsLog = millis() - debutLog;
        myFile.print(serie); myFile.print(","); myFile.print(tempsLog); myFile.print(",");myFile.println(AcX);     
        Serial.print(serie); Serial.print(" "); Serial.print(tempsLog); Serial.print(" AcY = "); Serial.println(AcX);
        //Serial.print(" | AcY = "); Serial.print(AcY);
        //Serial.print(" | AcZ = "); Serial.println(AcZ);
         
        i++;
        delay(cycle);
      }
      	// close the file:
       myFile.close();
       Serial.println("Fin des mesures");
    
        digitalWrite(10, HIGH);
        delay(3000);
        digitalWrite(10, LOW);
        delay(2000);
  
  }
